package me.randomhashtags.architect.util;

import com.sun.istack.internal.NotNull;
import me.randomhashtags.architect.ArchitectSpigot;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.scheduler.BukkitScheduler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public interface ArchitectUtilities {
    ArchitectSpigot architect = ArchitectSpigot.getPlugin;
    BukkitScheduler scheduler = Bukkit.getScheduler();
    //PluginManager pluginmanager = Bukkit.getPluginManager();
    ConsoleCommandSender console = Bukkit.getConsoleSender();

    Random random = new Random();
    File architectDataFolder = architect.getDataFolder();
    String separator = File.separator;

    Material[] materials = Material.values();
    //UMaterial[] umaterials = UMaterial.values();

    default Material getRandomBlockMaterial(List<String> excluding) {
        final int l = materials.length;
        Material m;
        final boolean nn = excluding != null;

        while (true) {
            m = materials[random.nextInt(l)];
            if(m.isBlock() && m.isSolid()) {
                if(nn) {
                    final List<Boolean> did = new ArrayList<>();
                    for(String s : excluding) {
                        did.add(m.name().endsWith(s.toUpperCase()));
                    }
                    if(!did.contains(true)) {
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        return m;
    }
    default BlockFace getRandomBlockFace() {
        switch (random.nextInt(6)) {
            case 0: return BlockFace.DOWN;
            case 1: return BlockFace.EAST;
            case 2: return BlockFace.NORTH;
            case 3: return BlockFace.SOUTH;
            case 4: return BlockFace.UP;
            case 5: return BlockFace.WEST;
            default: return null;
        }
    }

    default void save(String folder, String file) {
        final boolean hasFolder = folder != null && !folder.equals("");
        final File f = new File(architectDataFolder + separator + (hasFolder ? folder + separator : ""), file);
        if(!f.exists()) {
            f.getParentFile().mkdirs();
            architect.saveResource(hasFolder ? folder + separator + file : file, false);
        }
    }
    default void sendConsoleMessage(String msg) {
        console.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
    }

    default String toString(@NotNull Location loc) { return loc.getWorld().getName() + ";" + loc.getX() + ";" + loc.getY() + ";" + loc.getZ() + ";" + loc.getYaw() + ";" + loc.getPitch(); }
    default Location toLocation(@NotNull String string) {
        if(string.contains(";")) {
            final String[] a = string.split(";");
            return new Location(Bukkit.getWorld(a[0]), Double.parseDouble(a[1]), Double.parseDouble(a[2]), Double.parseDouble(a[3]), Float.parseFloat(a[4]), Float.parseFloat(a[5]));
        } else {
            return null;
        }
    }


}
