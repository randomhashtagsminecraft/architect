package me.randomhashtags.architect.addon;

import me.randomhashtags.architect.ArchitectStorage;
import me.randomhashtags.architect.addon.util.Identifiable;
import me.randomhashtags.architect.util.ArchitectUtilities;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.plugin.Plugin;

public abstract class ArchitectBlockPopulator extends BlockPopulator implements Identifiable, ArchitectUtilities {
    private boolean isEnabled = false;

    public void enable() {
        if(!isEnabled) {
            isEnabled = true;
            ArchitectStorage.registerBlockPopulator(getPlugin(), this);
            load();
        }
    }
    public void disable() {
        if(isEnabled) {
            isEnabled = false;
            ArchitectStorage.unregisterBlockPopulator(getPlugin(), this);
            unload();
        }
    }

    public abstract void load();
    public abstract void unload();
    public abstract Plugin getPlugin();
    public abstract YamlConfiguration getConfig();
}
