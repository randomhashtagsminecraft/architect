package me.randomhashtags.architect.addon;

import me.randomhashtags.architect.ArchitectStorage;
import me.randomhashtags.architect.addon.util.Configurable;
import me.randomhashtags.architect.addon.util.Identifiable;
import me.randomhashtags.architect.addon.util.Schematicable;
import me.randomhashtags.architect.addon.utils.Enableable;

public abstract class ArchitectTree extends Enableable implements Identifiable, Configurable, Schematicable {
    @Override
    public void enable() {
        if(!isEnabled) {
            isEnabled = true;
            ArchitectStorage.registerTree(getPlugin(), this);
            load();
        }
    }

    @Override
    public void disable() {
        if(isEnabled) {
            isEnabled = false;
            ArchitectStorage.unregisterTree(getPlugin(), this);
            unload();
        }
    }
}
