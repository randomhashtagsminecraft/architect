package me.randomhashtags.architect.addon.util;

import org.bukkit.configuration.file.YamlConfiguration;

public interface Configurable {
    YamlConfiguration getConfig();
}
