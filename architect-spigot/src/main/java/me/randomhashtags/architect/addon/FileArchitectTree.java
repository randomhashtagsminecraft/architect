package me.randomhashtags.architect.addon;

import com.sun.istack.internal.NotNull;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;

import static me.randomhashtags.architect.ArchitectSpigot.getPlugin;

public class FileArchitectTree extends ArchitectTree {
    private File f;
    private YamlConfiguration yml;
    public FileArchitectTree(@NotNull File f) {
        this.f = f;
        yml = YamlConfiguration.loadConfiguration(f);
        enable();
    }
    public void load() {
    }
    public void unload() {
    }

    public String getIdentifier() { return f.getName().split("\\.yml")[0]; }
    public Plugin getPlugin() { return getPlugin; }
    public YamlConfiguration getConfig() { return yml; }
    public File getSchematic() { return null; }
}
