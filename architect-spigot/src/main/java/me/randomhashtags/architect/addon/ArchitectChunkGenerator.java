package me.randomhashtags.architect.addon;

import java.util.List;
import java.util.Random;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;

public abstract class ArchitectChunkGenerator extends ChunkGenerator {
    public abstract ChunkGenerator.ChunkData generateChunkData(World world, Random random, int x, int z, ChunkGenerator.BiomeGrid biome);
    public abstract List<BlockPopulator> getDefaultPopulators(World world);
}
