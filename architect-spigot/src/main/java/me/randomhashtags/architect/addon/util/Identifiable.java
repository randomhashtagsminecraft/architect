package me.randomhashtags.architect.addon.util;

public interface Identifiable {
    String getIdentifier();
}
