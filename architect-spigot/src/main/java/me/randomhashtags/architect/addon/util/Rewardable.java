package me.randomhashtags.architect.addon.util;

import com.sun.istack.internal.NotNull;

import java.util.List;
import java.util.Random;

public interface Rewardable {
    String getRewardSize();
    default int getRandomRewardSize(@NotNull Random random) {
        final String[] values = getRewardSize().split("-");
        final int min = Integer.parseInt(values[0]);
        final int l = values.length;
        return l == 1 ? min : min+random.nextInt(Integer.parseInt(values[1])-min+1);
    }
    List<String> getRewards();
}
