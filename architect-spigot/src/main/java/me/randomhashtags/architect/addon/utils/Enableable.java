package me.randomhashtags.architect.addon.utils;

import org.bukkit.plugin.Plugin;

public abstract class Enableable {
    protected boolean isEnabled = false;

    public void enable() {
        if(!isEnabled) {
            isEnabled = true;
            load();
        }
    }
    public void disable() {
        if(isEnabled) {
            isEnabled = false;
            unload();
        }
    }

    public abstract void load();
    public abstract void unload();
    public abstract Plugin getPlugin();
}
