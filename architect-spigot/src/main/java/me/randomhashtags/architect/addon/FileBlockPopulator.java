package me.randomhashtags.architect.addon;

import me.randomhashtags.architect.addon.util.Schematicable;
import me.randomhashtags.architect.populators.AbstractBlockPopulator;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.Random;

public class FileBlockPopulator extends AbstractBlockPopulator implements Schematicable {
    private File f;
    private YamlConfiguration yml;

    public FileBlockPopulator(File f) {
        this.f = f;
        yml = YamlConfiguration.loadConfiguration(f);
    }
    public void load() {
    }
    public void unload() {
    }

    public String getIdentifier() { return f.getName().split("\\.yml")[0]; }
    public YamlConfiguration getConfig() { return yml; }

    public void populate(World world, Random random, Chunk chunk) {
    }

    public File getSchematic() {
        return null;
    }
}
