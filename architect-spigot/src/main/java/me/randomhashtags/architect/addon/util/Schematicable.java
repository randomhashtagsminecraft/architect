package me.randomhashtags.architect.addon.util;

import com.boydti.fawe.object.schematic.Schematic;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.extent.clipboard.Clipboard;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.world.registry.WorldData;
import com.sun.istack.internal.NotNull;
import org.bukkit.Location;

import java.io.File;
import java.io.FileInputStream;

public interface Schematicable {
    File getSchematic();
    default void paste(@NotNull Location l) throws Exception {
        final com.sk89q.worldedit.Vector to = new com.sk89q.worldedit.Vector(l.getBlockX(), l.getBlockY(), l.getBlockZ());
        final com.sk89q.worldedit.world.World W = new BukkitWorld(l.getWorld());
        final WorldData worldData = W.getWorldData();
        final Clipboard clipboard = ClipboardFormat.SCHEMATIC.getReader(new FileInputStream(getSchematic())).read(worldData);
        final Schematic s = new Schematic(clipboard);
        s.paste(W, to, false, true, null);
    }
}
