package me.randomhashtags.architect.addon;

import me.randomhashtags.architect.addon.util.Identifiable;
import me.randomhashtags.architect.addon.util.Rewardable;

import java.util.List;

public interface ArchitectLootTable extends Identifiable, Rewardable {
    List<String> getBlocksAffected();
    List<String> getBlacklistedBlocks();
}
