package me.randomhashtags.architect.addon.util;

public interface Enableable {
    void enable();
    void disable();
}
