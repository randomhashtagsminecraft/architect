package me.randomhashtags.architect;

import me.randomhashtags.architect.addon.util.Enableable;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;

import static me.randomhashtags.architect.util.ArchitectUtilities.scheduler;

public final class ArchitectSpigot extends JavaPlugin implements Enableable {
    public static ArchitectSpigot getPlugin;
    private long time = 0L;

    public void onEnable() {
        enable();
    }
    public void onDisable() {
        disable();
    }

    public void enable() {
        getPlugin = this;
        saveDefaultConfig();
    }
    public void disable() {
        ArchitectStorage.unregisterAllBlockPopulators();
    }


    private void checkTPS() {
        this.time = System.currentTimeMillis();
        scheduler.scheduleSyncRepeatingTask(this, () -> {
            long t = System.currentTimeMillis();
            System.out.println("tps=" + (t - this.time));
            this.time = t;
        }, 20L, 20L);
    }

    public ChunkGenerator getDefaultWorldGenerator(String world, String id) {
        return ArchitectGenerator.getArchitectGenerator();
    }
}
