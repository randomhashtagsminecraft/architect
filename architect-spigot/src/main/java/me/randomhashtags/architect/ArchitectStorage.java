package me.randomhashtags.architect;

import com.sun.istack.internal.NotNull;
import me.randomhashtags.architect.addon.ArchitectBlockPopulator;
import me.randomhashtags.architect.addon.ArchitectTree;
import me.randomhashtags.architect.util.ArchitectUtilities;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class ArchitectStorage implements ArchitectUtilities {
    private static HashMap<Plugin, List<ArchitectBlockPopulator>> blockPopulators = new HashMap<>();
    private static HashMap<String, ArchitectBlockPopulator> blockIdentifiers = new HashMap<>();

    private static HashMap<Plugin, List<ArchitectTree>> trees = new HashMap<>();
    private static HashMap<String, ArchitectTree> treeIdentifiers = new HashMap<>();

    public static void unregisterAll() {
        unregisterAllBlockPopulators();
        unregisterAllTrees();
    }

    public static void unregisterAllBlockPopulators() {
        for(ArchitectBlockPopulator bp : new ArrayList<>(blockIdentifiers.values())) {
            bp.disable();
        }
        blockPopulators = null;
        blockIdentifiers = null;
    }
    public static void unregisterBlockPopulator(@NotNull Plugin plugin, @NotNull ArchitectBlockPopulator populator) {
        if(blockPopulators.containsKey(plugin)) {
            blockPopulators.get(plugin).remove(populator);
            blockIdentifiers.remove(populator.getIdentifier());
        }
    }
    public static List<ArchitectBlockPopulator> getBlockPopulators(@NotNull String plugin) {
        for(Plugin p : blockPopulators.keySet()) {
            if(p.getName().equals(plugin)) {
                return blockPopulators.get(p);
            }
        }
        return null;
    }
    public static ArchitectBlockPopulator getBlockPopulator(@NotNull String identifier) {
        return blockIdentifiers.getOrDefault(identifier.toUpperCase(), null);
    }
    public static void registerBlockPopulator(@NotNull Plugin plugin, @NotNull ArchitectBlockPopulator populator) {
        if(!blockPopulators.containsKey(plugin)) {
            blockPopulators.put(plugin, new ArrayList<>());
        }
        blockPopulators.get(plugin).add(populator);
        blockIdentifiers.put(plugin.getName().toUpperCase() + "_" + populator.getIdentifier().toUpperCase(), populator);
    }

    public static void unregisterAllTrees() {
        for(List<ArchitectTree> list : trees.values()) {
            for(ArchitectTree tree : list) {
                tree.disable();
            }
        }
        trees = null;
        treeIdentifiers = null;
    }
    public static void unregisterTree(@NotNull Plugin plugin, @NotNull ArchitectTree tree) {
        if(trees.containsKey(plugin)) {
            trees.get(plugin).remove(tree);
            treeIdentifiers.remove(tree.getIdentifier());
        }
    }
    public static ArchitectTree getTree(@NotNull String identifier) {
        return treeIdentifiers.getOrDefault(identifier, null);
    }
    public static void registerTree(@NotNull Plugin plugin, @NotNull ArchitectTree tree) {
        if(!trees.containsKey(plugin)) {
            trees.put(plugin, new ArrayList<>());
        }
        trees.get(plugin).add(tree);
        treeIdentifiers.put(plugin.getName().toUpperCase() + "_" + tree.getIdentifier().toUpperCase(), tree);
    }
}
