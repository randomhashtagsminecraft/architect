package me.randomhashtags.architect.engine;

import com.sun.istack.internal.NotNull;
import me.randomhashtags.architect.addon.util.Identifiable;

import java.util.HashMap;

public abstract class AbstractKeyword implements Identifiable {
    public String getIdentifier() {
        final String[] n = getClass().getName().split("\\.");
        return n[n.length-1].toUpperCase();
    }
    public abstract void getFunction(@NotNull HashMap<String, Object> body);
}
