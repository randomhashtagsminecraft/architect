package me.randomhashtags.architect.tests;

import java.util.Random;
import me.randomhashtags.architect.addon.ArchitectBlockPopulator;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.World;

public abstract class SkyOrePopulator extends ArchitectBlockPopulator {
    public String getIdentifier() { return "SkyOre"; }
    public void populate(World world, Random random, Chunk chunk) {
        final Material air = Material.AIR;
        for(int i = 1; i < 15; i++) {
            if(random.nextInt(100) < 60) {
                int X = random.nextInt(15);
                int Z = random.nextInt(15);
                int Y = random.nextInt(100) + 100;
                if(chunk.getBlock(X, Y, Z).getType() == air) {
                    boolean isStone = true;
                    while (isStone) {
                        chunk.getBlock(X, Y, Z).setType(Material.COAL_ORE);
                        if(random.nextInt(100) < 40) {
                            switch (random.nextInt(5)) {
                                case 0:
                                    X++;
                                    break;
                                case 1:
                                    Y++;
                                    break;
                                case 2:
                                    Z++;
                                    break;
                                case 3:
                                    X--;
                                    break;
                                case 4:
                                    Y--;
                                    break;
                                case 5:
                                    Z--;
                            }
                            isStone = random.nextInt(4) <= 2;
                        } else {
                            isStone = false;
                        }
                    }
                }
            }
        }
    }
}
