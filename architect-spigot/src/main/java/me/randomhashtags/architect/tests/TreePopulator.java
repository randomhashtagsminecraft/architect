package me.randomhashtags.architect.tests;

import java.util.Random;
import me.randomhashtags.architect.addon.ArchitectBlockPopulator;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.TreeType;
import org.bukkit.World;

public abstract class TreePopulator extends ArchitectBlockPopulator {
    public String getIdentifier() { return "Tree"; }
    public void populate(World world, Random random, Chunk chunk) {
        if(random.nextBoolean()) {
            final int amount = random.nextInt(4)+1;
            final Material air = Material.AIR;
            final TreeType type = TreeType.TREE;
            for(int i = 1; i < amount; i++) {
                int X = random.nextInt(15),  Z = random.nextInt(15);
                for(int Y = world.getMaxHeight()-1; chunk.getBlock(X, Y, Z).getType() == air; Y--) {
                    world.generateTree(chunk.getBlock(X, Y, Z).getLocation(), type);
                }
            }
        }
    }
}
