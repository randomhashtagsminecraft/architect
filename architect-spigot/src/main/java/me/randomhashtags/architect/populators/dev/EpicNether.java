package me.randomhashtags.architect.populators.dev;

import me.randomhashtags.architect.populators.AbstractBlockPopulator;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.Random;

public final class EpicNether extends AbstractBlockPopulator {
    private static EpicNether instance;
    public static EpicNether getEpicNether() {
        if(instance == null) instance = new EpicNether();
        return instance;
    }
    private YamlConfiguration config;

    public void load() {
        final long started = System.currentTimeMillis();
        save("populators", "EpicNether.yml");
        config = YamlConfiguration.loadConfiguration(new File(architectDataFolder + separator + "populators", "EpicNether.yml"));
        sendConsoleMessage("&6[Architect] &aLoaded EpicNether &e(took " + (System.currentTimeMillis()-started) + "ms)");
    }
    public void unload() {
    }

    public String getIdentifier() { return "EpicNether"; }
    public YamlConfiguration getConfig() { return config; }

    public void populate(World world, Random random, Chunk chunk) {
    }
}
