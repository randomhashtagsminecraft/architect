package me.randomhashtags.architect.populators;

import me.randomhashtags.architect.addon.ArchitectBlockPopulator;
import me.randomhashtags.architect.util.ArchitectUtilities;
import org.bukkit.plugin.Plugin;

public abstract class AbstractBlockPopulator extends ArchitectBlockPopulator implements ArchitectUtilities {
    public Plugin getPlugin() { return architect; }
}
