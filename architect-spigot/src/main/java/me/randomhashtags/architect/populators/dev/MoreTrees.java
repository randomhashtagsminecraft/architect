package me.randomhashtags.architect.populators.dev;

import com.sun.istack.internal.NotNull;
import me.randomhashtags.architect.ArchitectStorage;
import me.randomhashtags.architect.addon.ArchitectTree;
import me.randomhashtags.architect.addon.FileArchitectTree;
import me.randomhashtags.architect.populators.AbstractBlockPopulator;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.Random;

public final class MoreTrees extends AbstractBlockPopulator {
    private static MoreTrees instance;
    public static MoreTrees getMoreTrees() {
        if(instance == null) instance = new MoreTrees();
        return instance;
    }
    private YamlConfiguration config;

    public void load() {
        final long started = System.currentTimeMillis();
        final String folder = "populators" + separator + "MoreTrees", schem = "schematics" + separator + "trees";
        save(folder, "_settings.yml");
        config = YamlConfiguration.loadConfiguration(new File(architectDataFolder + separator + "populators" + separator + "MoreTrees", "_settings.yml"));

        final String[] trees = new String[] {
                "Shwacked"
        };
        for(String s : trees) {
            save(schem, s + ".schematic");
            new FileArchitectTree(new File(schem, s + ".schematic"));
        }

        sendConsoleMessage("&6[Architect] &aLoaded MoreTrees &e(took " + (System.currentTimeMillis()-started) + "ms)");
    }
    public void unload() {
    }

    public String getIdentifier() { return "MoreTrees"; }
    public YamlConfiguration getConfig() { return config; }

    public void generateTree(@NotNull World w, Random random, Location l, @NotNull ArchitectTree type) {
        sendConsoleMessage("tried generating tree at " + l.toString());
        final Block existing = w.getBlockAt(l);
        //if(existing.getType().equals(Material.AIR)) {
            try {
                type.paste(l);
                sendConsoleMessage("pasted tree at " + l.toString());
            } catch (Exception e) {
                sendConsoleMessage("&6[Architect] &cERROR &etrying to generate Tree &f" + type.getIdentifier() + " &ein world &f" + w.getName() + " &eat &7" + l.getBlockX() + "x " + l.getBlockY() + "y " + l.getBlockZ() + "z&e!");
                e.printStackTrace();
            }
        //}
    }

    public void populate(World world, Random random, Chunk chunk) {
        /*
        for(int x = 0; x < 15; x++) {
            for(int z = 0; z < 15; z++) {
                chunk.getBlock(x, 0, z).setType(Material.BEDROCK);
            }
        }*/
        if(random.nextBoolean()) {
            final int amount = 1+random.nextInt(5);
            final ArchitectTree tree = ArchitectStorage.getTree("Shwacked");
            for(int i = 1; i <= amount; i++) {
                final int x = random.nextInt(15), z = random.nextInt(15);
                int y = 0;
                //for(y = world.getMaxHeight()-1; chunk.getBlock(x, y, z).getType() == Material.AIR; y--);
                generateTree(world, random, chunk.getBlock(x, 100, z).getLocation(), tree);
            }
        }
    }
}