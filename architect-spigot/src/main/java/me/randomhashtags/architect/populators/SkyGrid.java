package me.randomhashtags.architect.populators;

import com.sun.istack.internal.NotNull;
import me.randomhashtags.architect.populators.obj.SkyGridLootTable;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Container;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Directional;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class SkyGrid extends AbstractBlockPopulator {
    private static SkyGrid instance;
    public static SkyGrid getSkyGrid() {
        if(instance == null) instance = new SkyGrid();
        return instance;
    }
    private YamlConfiguration config;
    private List<String> blacklist, randomFacingDirection, randomAxis;
    private List<SkyGridLootTable> lootTables;

    public void load() {
        final long started = System.currentTimeMillis();
        save("populators", "SkyGrid.yml");
        config = YamlConfiguration.loadConfiguration(new File(architectDataFolder + separator + "populators", "SkyGrid.yml"));

        blacklist = new ArrayList<>();
        for(String s : config.getStringList("blacklisted blocks")) {
            blacklist.add(s.toUpperCase());
        }

        randomFacingDirection = new ArrayList<>();
        for(String s : config.getStringList("random facing direction")) {
            randomFacingDirection.add(s.toUpperCase());
        }

        randomAxis = new ArrayList<>();
        for(String s : config.getStringList("random axis")) {
            randomAxis.add(s.toUpperCase());
        }

        lootTables = new ArrayList<>();
        sendConsoleMessage("&6[Architect] &aLoaded SkyGrid &e(took " + (System.currentTimeMillis()-started) + "ms)");
    }
    public void unload() {
    }

    public String getIdentifier() { return "SkyGrid"; }
    public YamlConfiguration getConfig() { return config; }

    public void populate(World world, Random random, Chunk chunk) {
        for(int y = 0; y <= 256; y += 4) {
            for(int x = 0; x < 16; x += 4) {
                for(int z = 0; z < 16; z += 4) {
                    final Material m = getRandomBlockMaterial(blacklist);
                    final String material = m.name();
                    final Block b = chunk.getBlock(x, y, z);
                    b.setType(m, false);
                    if(isDirectional(material) && b instanceof Directional) {
                        ((Directional) b).setFacingDirection(getRandomBlockFace());
                    }
                    final SkyGridLootTable loottable = isLootable(material);
                    if(loottable != null && b instanceof Container) { // TODO: finish dis
                        final Container c = (Container) b;
                        final Inventory inv = c.getInventory();
                        final List<String> rewards = loottable.getRewards();
                        final int size = rewards.size();
                        final List<Integer> slots = new ArrayList<>();
                        for(int i = 0; i < inv.getSize(); i++) {
                            slots.add(i);
                        }
                        for(int i = 1; i <= loottable.getRandomRewardSize(random); i++) {
                            String reward = rewards.get(random.nextInt(size)), rl = reward.toLowerCase();
                            final boolean hasSlot = rl.contains(";slot=");
                            if(hasSlot) reward = reward.split(";slot=")[0];
                            ItemStack item = null;

                            if(item != null) {
                                final int slot = hasSlot ? Integer.parseInt(rl.split(";slot=")[1]) : slots.get(random.nextInt(slots.size()));
                                slots.remove((Object) slot);
                                inv.setItem(slot, item);
                            }
                        }
                        b.getState().update(true);
                    }
                }
            }
        }
    }

    private boolean isDirectional(@NotNull String material) {
        for(String s : randomFacingDirection) {
            if(material.endsWith(s)) {
                return true;
            }
        }
        return false;
    }
    private SkyGridLootTable isLootable(@NotNull String material) {
        for(SkyGridLootTable s : lootTables) {
            for(String b : s.getBlocksAffected()) {
                if(material.endsWith(b.toUpperCase())) {
                    return s;
                }
            }
        }
        return null;
    }
}
