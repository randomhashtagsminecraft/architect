package me.randomhashtags.architect.populators.obj;

import me.randomhashtags.architect.addon.ArchitectLootTable;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.List;

import static me.randomhashtags.architect.populators.SkyGrid.getSkyGrid;

public class SkyGridLootTable implements ArchitectLootTable {
    private String identifier;
    public SkyGridLootTable(String identifier) {
        this.identifier = identifier;
    }
    public String getIdentifier() { return identifier; }
    public List<String> getBlocksAffected() { return getConfig().getStringList("lootable." + identifier + ".blocks affected"); }
    public List<String> getBlacklistedBlocks() { return getConfig().getStringList("lootable." + identifier + ".blacklisted blocks"); }
    public String getRewardSize() { return getConfig().getString("lootable." + identifier + ".reward size"); }
    public List<String> getRewards() { return getConfig().getStringList("lootable." + identifier + ".rewards"); }

    private YamlConfiguration getConfig() { return getSkyGrid().getConfig(); }
}
