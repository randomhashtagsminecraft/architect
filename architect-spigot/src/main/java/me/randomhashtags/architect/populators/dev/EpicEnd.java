package me.randomhashtags.architect.populators.dev;

import me.randomhashtags.architect.populators.AbstractBlockPopulator;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.Random;

public final class EpicEnd extends AbstractBlockPopulator {
    private static EpicEnd instance;
    public static EpicEnd getEpicEnd() {
        if(instance == null) instance = new EpicEnd();
        return instance;
    }
    private YamlConfiguration config;

    public void load() {
        final long started = System.currentTimeMillis();
        save("populators", "EpicEnd.yml");
        config = YamlConfiguration.loadConfiguration(new File(architectDataFolder + separator + "populators", "EpicEnd.yml"));
        sendConsoleMessage("&6[Architect] &aLoaded EpicEnd &e(took " + (System.currentTimeMillis()-started) + "ms)");
    }
    public void unload() {
    }

    public String getIdentifier() { return "EpicEnd"; }
    public YamlConfiguration getConfig() { return config; }

    public void populate(World world, Random random, Chunk chunk) {
    }
}
