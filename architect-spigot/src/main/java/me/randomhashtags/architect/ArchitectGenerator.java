package me.randomhashtags.architect;

import me.randomhashtags.architect.addon.ArchitectBlockPopulator;
import me.randomhashtags.architect.addon.ArchitectChunkGenerator;
import me.randomhashtags.architect.populators.AbstractBlockPopulator;
import me.randomhashtags.architect.populators.SkyGrid;
import me.randomhashtags.architect.populators.dev.EpicEnd;
import me.randomhashtags.architect.populators.dev.EpicNether;
import me.randomhashtags.architect.populators.dev.MoreTrees;
import me.randomhashtags.architect.util.ArchitectUtilities;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.util.noise.SimplexOctaveGenerator;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

@SuppressWarnings("unchecked")
public final class ArchitectGenerator extends ArchitectChunkGenerator implements ArchitectUtilities {
    private static ArchitectGenerator instance;
    public static ArchitectGenerator getArchitectGenerator() {
        if(instance == null) {
            instance = new ArchitectGenerator();
            instance.load();
        }
        return instance;
    }

    private HashMap<String, HashMap<String, Object>> worlds;

    private void load() {
        save(null, "world settings.yml");
        worlds = new HashMap<>();
        final YamlConfiguration config = YamlConfiguration.loadConfiguration(new File(architectDataFolder, "world settings.yml"));
        for(String s : config.getConfigurationSection("").getKeys(false)) {
            worlds.put(s, new HashMap<>());
            final HashMap<String, Object> settings = worlds.get(s);
            for(String setting : config.getConfigurationSection(s).getKeys(false)) {
                settings.put(setting, config.get(s + "." + setting));
            }
        }

        final AbstractBlockPopulator[] list = new AbstractBlockPopulator[] {
                EpicEnd.getEpicEnd(),
                EpicNether.getEpicNether(),
                MoreTrees.getMoreTrees(),
                SkyGrid.getSkyGrid()
        };

        for(AbstractBlockPopulator p : list) {
            p.enable();
        }
    }

    public ChunkGenerator.ChunkData generateChunkData(World world, Random random, int chunkX, int chunkZ, ChunkGenerator.BiomeGrid biome) {
        final SimplexOctaveGenerator generator = new SimplexOctaveGenerator(new Random(world.getSeed()), 8);
        final ChunkGenerator.ChunkData chunk = createChunkData(world);
        generator.setScale(0.005D);
        return chunk;
    }

    public List<BlockPopulator> getDefaultPopulators(World world) {
        final List<BlockPopulator> list = new ArrayList<>();
        final String n = world.getName();
        if(worlds.containsKey(n)) {
            final HashMap<String, Object> settings = worlds.get(n);
            for(String id : (List<String>) settings.get("populators")) {
                final ArchitectBlockPopulator p = ArchitectStorage.getBlockPopulator(id);
                if(p != null) {
                    list.add(p);
                }
            }
        }
        return !list.isEmpty() ? list : null;
    }
}
